$(document).ready(function () {

    // Global

    $('a').on("click", function (e) {
        if ($(this).attr('href') == '') {
            e.preventDefault();
        } else {
            return true;
        }
    });

    $('.close').click(function () {
        UIkit.offcanvas.hide();
    });

    //плавный скролл между якорями
    $('.smoothScroll').click(function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var target = $(href);
        var top = target.offset().top;
        $('html,body').animate({scrollTop: top}, 1000);
    });

    $('input[type=tel]').inputmask('+7(999)999-99-99').attr('pattern', '[+7()0-9-]{5,20}');

    //Кнопка вверх
    var scrollUp = document.getElementById('scrollup');
    scrollUp.onclick = function () {
        jQuery('html, body').animate({scrollTop: 0}, 500);
        return false;
    };

    window.onscroll = function () {
        if (window.pageYOffset > 0) {
            scrollUp.style.display = 'block';
        } else {
            scrollUp.style.display = 'none';
        }
    };


    $('.case-carousel').owlCarousel({
        loop: true,
        margin: 0,
        nav: true,
        dots: true,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000
    });
    $('.owl-prev').html('<i class="fa fa-angle-left">');
    $('.owl-next').html('<i class="fa fa-angle-right">');



});

